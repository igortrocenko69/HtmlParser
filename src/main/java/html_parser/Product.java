package html_parser;

import java.util.List;

/**
 * Created by Ihor on 31.12.2016.
 */
public class Product {
    private int id;
    private String name;
    private String url;
    private String price;
    private String brief;
    private String description;
    private String category;
    private String logo;
    private List<String> images;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<String> getImages() { return images;}

    public void setImages(List<String> images) { this.images = images;}

    public String getLogo() {return logo;}

    public void setLogo(String logo) {this.logo = logo;}

    @Override
    public String toString() {
        return "html_parser.Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", price='" + price + '\'' +
                ", brief='" + brief + '\'' +
                ", description='" + description + '\'' +
                ", category='" + category + '\'' +
                ", logo='" + logo + '\'' +
                ", images=" + images +
                '}';
    }
}
