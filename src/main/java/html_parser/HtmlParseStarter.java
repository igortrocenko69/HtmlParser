package html_parser;

import com.google.gson.Gson;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Ihor on 22.01.2017.
 * This class parsing http://best-horse.com/ and recording to json
 * using Jsoup library and Executors
 */
public class HtmlParseStarter {

    private static final String mainLink = "http://best-horse.com/";
    private static final String jsonPath = "D:\\JavaFreelance\\HTMLparsing\\";


    public static void main(String[] args) throws IOException {
        long startAllTime = System.currentTimeMillis();
        Document doc = Jsoup.connect(mainLink).get();
        Elements mainCategoryLinks = doc.select("#menus>ul>li>ul>li>a[href]");
        int mainCategoryLinksSize = mainCategoryLinks.size();
        System.out.println("Общее количество ссылок-"+mainCategoryLinksSize);

        Scanner sc = new Scanner(System.in);
        byte threadNumber;
        do {
            System.out.println("Введите число потоков от 1 до 100");
            threadNumber = sc.nextByte();
        } while (threadNumber < 1 || threadNumber > 100);

        int LinkNumberInThread = mainCategoryLinksSize / threadNumber;

        ExecutorService executorService = Executors.newFixedThreadPool(threadNumber);

        // List<html_parser.HtmlParser> parserThreads=new ArrayList<>();          //коллекция потоков

        for (int i = 0; i < threadNumber; i++) {
            if (i != (threadNumber - 1)) {
                List<Element> listMainCategoryLinks = mainCategoryLinks.subList(LinkNumberInThread * i, LinkNumberInThread * (i + 1));

                System.out.println("Список ссылок для потока №"+i);
                for(Element subListLink:listMainCategoryLinks){
                    System.out.println(subListLink);
                }
                System.out.println("Количество ссылок-"+listMainCategoryLinks.size());

                HtmlParser parserThread = new HtmlParser(listMainCategoryLinks);
                //  parserThreads.add(parserThread);
                executorService.execute(parserThread);

            } else {
                List<Element> listMainCategoryLinks = mainCategoryLinks.subList(LinkNumberInThread * i, mainCategoryLinksSize);

                System.out.println("Список ссылок для потока №"+i);
                for(Element subListLink:listMainCategoryLinks){
                    System.out.println(subListLink);
                }
                System.out.println("Количество ссылок-"+listMainCategoryLinks.size());
                HtmlParser parserThread = new HtmlParser(listMainCategoryLinks);
                executorService.execute(parserThread);
            }
        }

        executorService.shutdown();
        boolean terminated = false;
        while (!terminated) {
            try {
                Thread.sleep(1000);
                terminated = executorService.isTerminated();
            } catch (InterruptedException e) {
                System.out.println("Abort the main thread");
            }
        }


        try (FileWriter fileWriter = new FileWriter(jsonPath + "json_thread_12.txt")) {
            Gson gson = new Gson();
            gson.toJson(HtmlParser.getAllProducts(), fileWriter);
            System.out.println("json записан");
        } catch (FileNotFoundException e) {
            System.out.println("Ошибка записи json");
        }

        System.out.println("Общее число товаров=" + HtmlParser.getAllCountProduct());
        long endAllTime = System.currentTimeMillis();
        long allTime = endAllTime - startAllTime;
        System.out.println("Полное время-" + allTime + "мс");

    }
}
