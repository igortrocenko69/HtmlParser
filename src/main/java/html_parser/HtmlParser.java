package html_parser;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ihor on 24.12.2016.
 */
public class HtmlParser implements Runnable {
    private static int allCountProduct = 0;
    private static final int TIMEOUT = 10 * 10000;
    private static final String path = "D:\\JavaFreelance\\HTMLparsing\\all_pictures_thread_12\\";
    private Thread thread;
    private List<Element> mainCategoryLinks;
    private static List<Product> allProducts = new ArrayList<>();


    HtmlParser(List<Element> mainCategoryLinks) {
        this.mainCategoryLinks = mainCategoryLinks;
        thread = new Thread(this);
    }


    @Override
    public void run() {

        try {
            for (Element element : mainCategoryLinks) {
                long startCategoryRunTime = System.currentTimeMillis();
                String mainCategoryLink = element.attr("href");
                Document mainCategoryContent = downnloadFromLink(mainCategoryLink);
                List<Product> result = getProductsFromCategory(mainCategoryContent);
                fillAllProduct(result);
                long endCategoryRunTime = System.currentTimeMillis();
                long categoryRunTime = endCategoryRunTime - startCategoryRunTime;
                System.out.println("Время категории-" + mainCategoryLink + " : " + categoryRunTime + "мс");
            }

        } catch (IOException ex) {
            ex.printStackTrace();
            System.out.println("Ошибка ввода/вывода");

        }
    }

    private List<Product> getProductsFromCategory(Document mainCategoryContent) throws IOException {
        List<Product> result = new ArrayList<>();
        Elements subCategoriesLinks = mainCategoryContent.select("#content>div.category-list>ul>li>a[href]");//подкатегории
        if (subCategoriesLinks.isEmpty()) {                                  //если нет подкатегорий
            result = getProducts(mainCategoryContent);
        } else {                                                                //если есть подкатегории
            for (Element subCategoryLink : subCategoriesLinks) {
                String subCategoryUrl = subCategoryLink.attr("href");
                Document subCategoryContent = downnloadFromLink(subCategoryUrl);
                result.addAll(getProducts(subCategoryContent));
            }
        }

        return result;
    }

    private List<Product> getProducts(Document doc) throws IOException {
        //extract method
        List<String> productPagesLinks = getPageLinks(doc);
        List<Product> products = processProductPage(doc);              //товары с первой страницы
        for (String pageLink : productPagesLinks) {
            Document pageContent = downnloadFromLink(pageLink);
            List<Product> pageProducts = processProductPage(pageContent);
            products.addAll(pageProducts);                            //товары с остальных страниц
        }

        return products;
    }

    private Document downnloadFromLink(String link) throws IOException {
        return Jsoup.connect(link)
                .userAgent("Mozilla")
                .timeout(TIMEOUT)
                .get();


    }

    private List<String> getPageLinks(Document doc) {
        List<String> productPagesLinks = new ArrayList<>();            //получение ссылок на страницы с товарами         //добавляю в коллекцию ссылку на первую страницу
        Elements linkHttpPages = doc.select("div.pagination>div.links>a");
        for (Element linkHttpPage : linkHttpPages) {
            if (StringUtils.isNumeric(linkHttpPage.text())) {                          //если есть несколько страниц
                String linkHttpPageStr = linkHttpPage.attr("href");  //получаю ссылки на них
                productPagesLinks.add(linkHttpPageStr);                               //и добавляю в коллекцию
            }
        }
        System.out.println(productPagesLinks);

        return productPagesLinks;
    }

    private List<Product> processProductPage(Document doc) throws IOException {
        Elements divProducts = doc.select("div.product-list>div"); //перебор товаров в категории

        List<Product> products = new ArrayList<>();
        int count = 0;
        String category = "";
        for (Element divProduct : divProducts) {
            String url = divProduct.select("div.name>a[href]").first().attr("href"); //ссылка
            String name = divProduct.select("div.name>a[href]").first().text();   //название
            String brief = divProduct.select("div.description").first().text();   //кор.описание
            String price = divProduct.select("div.price").first().text();         //цена

            int id = Integer.parseInt(url.substring(url.indexOf("product_id=") + 11));    //id

            category = getCategory(doc);                                           //каталог товара

            Document productPage = downnloadFromLink(url);                               //страница конкретного товара

            String description = getDscription(productPage);                             //полное описание товара

            String fullPath = path + id;
            File dirPictures = new File(fullPath);
            dirPictures.mkdirs();                             //создание на диске каталога для хранения изображений

            String srcLink = divProduct.select("div.image>a>img[src]").first().attr("src");
            String logo = srcLink.substring(srcLink.lastIndexOf("/") + 1);  //название основного изображения
            saveToFile(srcLink, logo, fullPath);                                 //изображение в файл

            List<String> images = getImages(productPage, fullPath);                //названия других изображений

            Product product = new Product();
            product.setId(id);
            product.setName(name);
            product.setUrl(url);
            product.setPrice(price);
            product.setBrief(brief);
            product.setDescription(description);
            product.setCategory(category);
            product.setLogo(logo);
            product.setImages(images);

            products.add(product);

            count = count + 1;
        }
        System.out.println("число товаров на странице категории-" + category + "=" + count);
        fillAllCountProduct(count);

        return products;
    }

    private String getCategory(Document doc) {
        Elements categoriesList = doc.select("div.breadcrumb>a[href]");
        StringBuilder productCategory = new StringBuilder();
        for (Element subCategory : categoriesList) {
            String subCategoryStr = subCategory.text();
            productCategory.append(subCategoryStr).append("/");
        }
        return productCategory.toString();
    }

    private String getDscription(Document productPage) {
        Elements divDescriptions = productPage.select("#tab-description>div");
        StringBuilder productDescription = new StringBuilder();
        for (Element divDescription : divDescriptions) {
            String divDescriptionStr = divDescription.text();
            productDescription.append(divDescriptionStr).append("\n");
        }
        return productDescription.toString();
    }

    private List<String> getImages(Document productPage, String fullPath) throws IOException {
        Elements imagesList = productPage.select("div.image-additional>a");//получение других изображений
        List<String> images = new ArrayList<>();

        for (Element image : imagesList) {
            String imageLargeLink = image.select("[href]").first().attr("href");    //URL
            String imageSmallLink = image.select("a>img[src]").first().attr("src"); //картинок

            String imageLargeName = imageLargeLink.substring(imageLargeLink.lastIndexOf("/") + 1);  //названия
            String imageSmallName = imageSmallLink.substring(imageSmallLink.lastIndexOf("/") + 1);  //картинок
            images.add(imageLargeName);
            images.add(imageSmallName);
            saveToFile(imageLargeLink, imageLargeName, fullPath);
            saveToFile(imageSmallLink, imageSmallName, fullPath);
        }
        return images;
    }

    private void saveToFile(String urlImage, String fileName, String path) throws IOException {
        byte[] data = Jsoup.connect(urlImage)                         //загрузка изображения
                .userAgent("Mozilla")
                .timeout(TIMEOUT)
                .ignoreContentType(true)
                .execute()
                .bodyAsBytes();
        try (FileOutputStream fos = new FileOutputStream(path + "\\" + fileName)) {
            fos.write(data);                                                       //запись изображения в файл
            // System.out.println("Файл записан");
        } catch (FileNotFoundException e) {
            System.out.println("Ошибка записи в файл");
        }

    }

    private synchronized void fillAllCountProduct(int count) {
        allCountProduct = allCountProduct + count;
    }

    private synchronized void fillAllProduct(List<Product> result) {
        allProducts.addAll(result);
    }

    static int getAllCountProduct() {
        return allCountProduct;
    }

    static List<Product> getAllProducts() {
        return allProducts;
    }

    public Thread getThread() {
        return thread;
    }
}
